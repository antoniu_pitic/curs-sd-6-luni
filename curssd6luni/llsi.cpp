// LLSI

#include <iostream>
#include <time.h>
using namespace std;

struct nod{
	int val;
	nod *leg;
};



void afis(nod *cap){
	cout << endl;
	for(nod *p=cap;p;p=p->leg){
		cout << p->val << " ";
	}
}

void ad_inc(nod *&cap ,int x){
	nod *p=new nod;
	p->val = x;
	p->leg = cap;
	cap = p;
}

void creare(nod *&cap){
	cap=NULL;
}



void ad_sf(nod *&cap,int x){
	nod *p;
	if (cap!=NULL){
		// mergi pe ultimul
		for(p=cap;p->leg;p=p->leg);

		// adauga dupa el
		nod *q=new nod;
		q->val = x;
		q->leg = NULL;
		p->leg =q;
	}
	else{
		ad_inc(cap,x);
	}
}
	
nod* caut(nod* cap,int x){
	for(nod *p=cap;p;p=p->leg){
		if (p->val == x){
			return p;
		}
	}	
	return NULL;
}


void st_inc(nod *&cap){
	if (cap==NULL){
		cout << "NAMCE";
	}
	else{
		nod *fostuCap = cap;
		cap =cap->leg;
		delete fostuCap;
	}
}


void st_sf(nod *&cap){
	if (cap==NULL || cap->leg==NULL){
		st_inc(cap);
	}
	else{
		// mergi pe penultimul
		nod *p;
		for(p=cap;p->leg->leg;p=p->leg);

		delete p->leg;
		p->leg = NULL;
	}
}

void st_val(nod *&cap,int x){
	if (caut(cap,x)==NULL){
		cout << "NAMCE";
	}
	else{
		if (cap->val == x){
			st_inc(cap);
		}
		else{
			nod *p,*q;
			for(p=cap; p->leg->val != x; p=p->leg);
			q=p->leg;
			p->leg = q->leg;
			delete q;
		}
	}
}

int cateValori(nod *cap){
	int ct=0;
	for(nod *p=cap;p;p=p->leg){
		ct++;
	}
	return ct;
}

void sort(nod *cap){
	int n = cateValori(cap);
	for(int t=1;t<=n-1;t++){
		for(nod *p=cap;p->leg;p=p->leg){
			if (p->val > p->leg->val){
				swap(p->val , p->leg->val);
			}
		}
	}
}




void ad_ord(nod *&cap,int x){
	if (cap==NULL || x < cap->val){
		ad_inc(cap,x);
	}
	else{
		nod *p,*q;
		for(p=cap;p->leg && p->leg->val < x; p=p->leg);
		q=new nod;
		q->val = x;
		q->leg = p->leg;
		p->leg = q;
	}
}