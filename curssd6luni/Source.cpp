// LLDI

#include <iostream>
using namespace std;

struct nod{
	int val;
	nod *ls,*ld;
};



void afis(nod *cap){
	for(nod *p=cap;p;p=p->ld){
		cout << p->val << " ";
	}
	cout << endl;
}

void afis_inv(nod *cap){
	nod *p;
	for(p=cap;p->ld;p=p->ld);

	for(;p;p=p->ls){
		cout << p->val << " ";
	}
	cout << endl;
}
void creare(nod *&cap){
	cap=NULL;
}

void ad_inc(nod *&cap ,int x){
	if (cap==NULL){
		cap = new nod;
		cap->val = x;
		cap->ls = cap->ld = NULL;
	}
	else{
		nod *p=new nod;
		p->val = x;
		p->ls = NULL;
		p->ld = cap;
		cap->ls= p;
		cap=p;
	}
}

nod * caut(nod *cap,int x){
	for(nod *p=cap;p;p=p->ld){
		if (p->val == x){
			return p;
		}
	}
	return NULL;
}

// stergere doar daca valoare e in mijloc
void sterg(nod *&cap,int x){
	nod *p;
	if (p=caut(cap,x)){
		p->ls->ld = p->ld;
		p->ld->ls = p->ls;
		delete p;
	}
	else{
		cout << "Namce";
	}
}

int main(){
	nod *cap;
	creare(cap);

	for(int i=1;i<=10;i++){
		ad_inc(cap,i);
	}

	afis(cap);
	afis_inv(cap);

	sterg(cap,5);

	afis_inv(cap);
}